package com.wirecard.sms;

import com.wirecard.sms.exception.InvalidSMSCommandException;
import lombok.Getter;

import java.util.Arrays;

@Getter
public enum SMSCommand {

    BALANCE,
    SEND,
    TOTAL;

    public static SMSCommand of(String input) throws InvalidSMSCommandException {

        return Arrays.stream(values())
                .filter(s -> input.startsWith(s.name()))
                .findFirst()
                .orElseThrow(() -> new InvalidSMSCommandException(input));

    }
}
