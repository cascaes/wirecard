package com.wirecard.sms;

import com.wirecard.sms.exception.InvalidSMSCommandException;
import com.wirecard.sms.strategy.GenericStrategy;
import com.wirecard.sms.strategy.SMSStatus;
import com.wirecard.sms.strategy.balance.BalanceStrategy;
import com.wirecard.sms.strategy.send.SendStrategy;
import com.wirecard.sms.strategy.total.TotalStrategy;
import com.wirecard.transactions.TransferManager;
import com.wirecard.users.UserManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SMSHandlerImpl implements SMSHandler {

    @Autowired
    private UserManager userManager;

    @Autowired
    private TransferManager transferManager;

    @Override
    public String handleSmsRequest(String smsContent, String senderDeviceId) {
        try {

            GenericStrategy strategy = null;
            SMSCommand smsCommand = SMSCommand.of(smsContent);
            log.info("SMS Command parsed: " + smsCommand);
            switch (smsCommand) {
                case TOTAL:
                    strategy = new TotalStrategy(smsContent, userManager, transferManager);
                    break;
                case BALANCE:
                    strategy = new BalanceStrategy(smsContent, userManager);
                    break;
                case SEND:
                    strategy = new SendStrategy(smsContent, userManager, transferManager);
                    break;
            }
            return strategy.execute(senderDeviceId);
        } catch (InvalidSMSCommandException e) {
            log.error("Invalid SMS Command: " + e.getMessage());
            return SMSStatus.UNKNOWN.getMessage();
        }
    }
}
