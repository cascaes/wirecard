package com.wirecard.sms;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@UtilityClass
public class SMSUtils {
    public BigDecimal parseAmount(String smsContent) {
        String[] pieces = splitContent(smsContent);
        return new BigDecimal(pieces[1]);
    }

    public Set<String> parseRecipients(String smsContent) {
        String[] pieces = splitContent(smsContent);
        return new LinkedHashSet<>(Arrays.asList(pieces).subList(2, pieces.length));
    }

    private String[] splitContent(String smsContent) {
        return smsContent.split("\\-");
    }
}
