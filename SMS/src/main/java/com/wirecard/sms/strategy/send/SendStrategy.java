package com.wirecard.sms.strategy.send;

import com.wirecard.sms.SMSUtils;
import com.wirecard.sms.strategy.GenericStrategy;
import com.wirecard.sms.strategy.SMSStatus;
import com.wirecard.transactions.TransferManager;
import com.wirecard.users.UserManager;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class SendStrategy extends GenericStrategy {

    private UserManager userManager;

    private TransferManager transferManager;

    public SendStrategy(String smsContent, UserManager userManager, TransferManager transferManager) {
        super(smsContent);
        this.userManager = userManager;
        this.transferManager = transferManager;
    }

    @Override
    public String execute(String deviceId) {
        SendData sendData = SendData.builder()
                .amount(SMSUtils.parseAmount(smsContent))
                .recipients(SMSUtils.parseRecipients(smsContent))
                .build();

        for (String recipient : sendData.getRecipients()) {
            if (!userManager.existsUser(recipient)) {
                log.warn("NO USER OCCURRENCE: " + recipient);
                return SMSStatus.NO_USER.getMessage();
            }
        }

        String sender = userManager.getUserNameForDeviceId(deviceId);
        BigDecimal funds = userManager.getBalance(sender);
        BigDecimal totalOfRecipients = new BigDecimal(sendData.getRecipients().size());
        BigDecimal totalToSend = sendData.getAmount().multiply(totalOfRecipients);

        if (funds.subtract(totalToSend).intValue() < 0) {
            log.warn("INSUFFICIENT FUNDS OCCURRENCE");
            return SMSStatus.INSUFFICIENT_FUNDS.getMessage();
        }

        for (String userName : sendData.getRecipients()) {
            transferManager.sendMoney(sender, userName, sendData.getAmount());
        }

        return SMSStatus.OK.getMessage();
    }
}
