package com.wirecard.sms.strategy;

import lombok.AllArgsConstructor;


@AllArgsConstructor
public abstract class GenericStrategy implements ActionStrategy {
    protected String smsContent;
}
