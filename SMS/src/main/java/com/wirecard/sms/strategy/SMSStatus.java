package com.wirecard.sms.strategy;

import lombok.Getter;

@Getter
public enum SMSStatus {

    UNKNOWN("ERR – UNKNOWN COMMAND"),
    NO_USER("ERR – NO USER"),
    INSUFFICIENT_FUNDS("ERR – INSUFFICIENT FUNDS"),
    OK("OK");

    private final String message;

    SMSStatus(String message) {

        this.message = message;
    }
}
