package com.wirecard.sms.strategy.send;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Builder
public class SendData {
    private BigDecimal amount;
    private Set<String> recipients;
}
