package com.wirecard.sms.strategy.total;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class TotalRecipient {
    private String recipient;
    private List<BigDecimal> transactions;

    public String getSumOfTransactions() {
        BigDecimal sum = BigDecimal.ZERO;
        for (BigDecimal transaction : transactions) {
            sum = sum.add(transaction);
        }
        return String.valueOf(sum.intValue());
    }
}
