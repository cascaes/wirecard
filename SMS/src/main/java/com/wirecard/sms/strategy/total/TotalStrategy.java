package com.wirecard.sms.strategy.total;

import com.wirecard.sms.SMSUtils;
import com.wirecard.sms.strategy.GenericStrategy;
import com.wirecard.sms.strategy.SMSStatus;
import com.wirecard.transactions.TransferManager;
import com.wirecard.users.UserManager;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class TotalStrategy extends GenericStrategy {

    private UserManager userManager;

    private TransferManager transferManager;

    public TotalStrategy(String smsContent, UserManager userManager, TransferManager transferManager) {
        super(smsContent);
        this.userManager = userManager;
        this.transferManager = transferManager;
    }

    @Override
    public String execute(String deviceId) {
        TotalData totalData = TotalData.builder()
                .recipients(SMSUtils.parseRecipients(smsContent))
                .build();
        String sender = userManager.getUserNameForDeviceId(deviceId);

        List<TotalRecipient> recipientHistory = new ArrayList<>();

        for (String recipient : totalData.getRecipients()) {
            if (userManager.existsUser(recipient)) {
                TotalRecipient totalRecipient = new TotalRecipient();
                totalRecipient.setRecipient(recipient);
                totalRecipient.setTransactions(transferManager.getAllTransactions(sender, recipient));
                recipientHistory.add(totalRecipient);
            } else {
                log.warn("NO USER OCCURRENCE: " + recipient);
                return SMSStatus.NO_USER.getMessage();
            }

        }
        return String
                .join(",", recipientHistory
                        .stream()
                        .map(TotalRecipient::getSumOfTransactions)
                        .collect(Collectors.toList())
                );

    }
}
