package com.wirecard.sms.strategy;

public interface ActionStrategy {
    String execute(String deviceId);
}
