package com.wirecard.sms.strategy.balance;

import com.wirecard.sms.strategy.GenericStrategy;
import com.wirecard.users.UserManager;

public class BalanceStrategy extends GenericStrategy {

    private final UserManager userManager;

    public BalanceStrategy(String smsContent, UserManager userManager) {
        super(smsContent);
        this.userManager = userManager;
    }

    @Override
    public String execute(String deviceId) {
        String sender = userManager.getUserNameForDeviceId(deviceId);

        return String.valueOf(userManager.getBalance(sender).intValue());
    }
}
