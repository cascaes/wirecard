package com.wirecard.sms.strategy.total;

import lombok.Builder;
import lombok.Getter;

import java.util.Set;

@Builder
@Getter
public class TotalData {
    private Set<String> recipients;
}
