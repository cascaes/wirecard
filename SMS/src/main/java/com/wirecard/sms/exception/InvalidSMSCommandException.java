package com.wirecard.sms.exception;

public class InvalidSMSCommandException extends Exception {
    public InvalidSMSCommandException(String input) {
        super(input);
    }
}
