package com.wirecard.users;

import com.wirecard.TestScenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

public class UserManagerImplTest extends TestScenario {

    @Autowired
    private UserManager userManager;

    @Test
    public void existUserTest() {
        boolean r = userManager.existsUser("TEST1");

        assertTrue(r);
    }

    @Test
    public void notExistUserTest() {
        boolean r = userManager.existsUser("WHOARTTHOU");

        assertFalse(r);
    }

    @Test
    public void balanceUserTEST1Test() {
        BigDecimal b = userManager.getBalance("TEST1");

        assertEquals(b.intValue(), new BigDecimal(3827).intValue());
    }

    @Test
    public void balanceUserInexistentTest() {
        BigDecimal b = userManager.getBalance("TEST0");

        assertEquals(b.intValue(), new BigDecimal(0).intValue());

    }

    @Test
    public void getUsernameByDeviceIdTest() {
        String un = userManager.getUserNameForDeviceId("D1");

        assertEquals("TEST1", un);
    }

    @Test
    public void nonExistingDeviceIdTest() {
        String un = userManager.getUserNameForDeviceId("D0");

        assertNull(un);
    }
}