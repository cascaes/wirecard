package com.wirecard.sms;


import com.wirecard.TestScenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class SMSHandlerImplTest extends TestScenario {

    @Autowired
    private SMSHandler smsHandler;

    @Test
    public void balanceTest() {
        String r = smsHandler.handleSmsRequest("BALANCE", "D1");

        assertEquals(r, "3827");
    }

    @Test
    public void sendOkTest() {
        String r = smsHandler.handleSmsRequest("SEND-100-TEST1", "D2");

        assertEquals(r, "OK");
    }

    @Test
    public void sendInsuficientFundTest() {
        String r = smsHandler.handleSmsRequest("SEND-10000000-TEST1", "D2");

        assertEquals(r, "ERR – INSUFFICIENT FUNDS");
    }

    @Test
    public void sendInexistentUserTest() {
        String r = smsHandler.handleSmsRequest("SEND-100-TEST999", "D2");

        assertEquals(r, "ERR – NO USER");
    }

    @Test
    public void totalSendTest() {
        String r = smsHandler.handleSmsRequest("TOTAL-SENT-TEST1", "D2");

        assertEquals(r, "100");
    }

    @Test
    public void totalSendMultipleTest() {
        String r = smsHandler.handleSmsRequest("TOTAL-SENT-TEST2-TEST3", "D1");

        assertEquals(r, "3091,82");
    }

    @Test
    public void totalSendNoUserTest() {
        String r = smsHandler.handleSmsRequest("TOTAL-SENT-TEST2-TEST8", "D1");

        assertEquals(r, "ERR – NO USER");
    }

    @Test
    public void unknownCommandTest() {
        String r = smsHandler.handleSmsRequest("FREE-TEST1", "D1");

        assertEquals(r, "ERR – UNKNOWN COMMAND");
    }



}