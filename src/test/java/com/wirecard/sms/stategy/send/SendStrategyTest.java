package com.wirecard.sms.stategy.send;

import com.wirecard.sms.stategy.MockStrategyTest;
import com.wirecard.sms.strategy.SMSStatus;
import com.wirecard.sms.strategy.send.SendStrategy;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class SendStrategyTest extends MockStrategyTest {

    @Test
    public void happyWaySendTest() {

        when(userManager.existsUser(MSMITH)).thenReturn(true);

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.getBalance(ITSME)).thenReturn(new BigDecimal(400));

        strategy = new SendStrategy("SEND-200-MSMITH", userManager, transferManager);

        assertEquals(strategy.execute(A12), SMSStatus.OK.getMessage());

        verify(transferManager).sendMoney(ITSME, MSMITH, new BigDecimal(200));

        verify(userManager).existsUser(MSMITH);

    }

    @Test
    public void insufficientFundSendTest() {

        when(userManager.existsUser(MSMITH)).thenReturn(true);

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.getBalance(ITSME)).thenReturn(new BigDecimal(100));

        strategy = new SendStrategy("SEND-200-MSMITH", userManager, transferManager);

        assertEquals(strategy.execute(A12), SMSStatus.INSUFFICIENT_FUNDS.getMessage());

        verifyZeroInteractions(transferManager);

    }

    @Test
    public void noUserSendTest() {

        when(userManager.existsUser(MSMITH)).thenReturn(false);

        strategy = new SendStrategy("SEND-200-MSMITH", userManager, transferManager);

        String r = strategy.execute(A12);

        assertEquals(r, SMSStatus.NO_USER.getMessage());

        verifyZeroInteractions(transferManager);
    }
}
