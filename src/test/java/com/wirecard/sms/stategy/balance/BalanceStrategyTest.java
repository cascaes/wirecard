package com.wirecard.sms.stategy.balance;

import com.wirecard.sms.stategy.MockStrategyTest;
import com.wirecard.sms.strategy.balance.BalanceStrategy;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class BalanceStrategyTest extends MockStrategyTest {

    @Test
    public void happyWayBalanceTest() {

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.getBalance(ITSME)).thenReturn(new BigDecimal(9833));

        strategy = new BalanceStrategy("BALANCE", userManager);

        assertEquals(strategy.execute(A12), "9833");
    }

    @Test
    public void oddDeviceIdBalanceTest() {

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(null);

        when(userManager.getBalance(null)).thenReturn(new BigDecimal(0));

        strategy = new BalanceStrategy("BALANCE", userManager);

        assertEquals(strategy.execute(A12), "0");
    }
}
