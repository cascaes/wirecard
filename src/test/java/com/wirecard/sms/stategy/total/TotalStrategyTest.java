package com.wirecard.sms.stategy.total;

import com.wirecard.sms.stategy.MockStrategyTest;
import com.wirecard.sms.strategy.SMSStatus;
import com.wirecard.sms.strategy.total.TotalStrategy;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TotalStrategyTest extends MockStrategyTest {

    @Test
    public void happyWayTest() {

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.existsUser(MSMITH)).thenReturn(true);

        List<BigDecimal> list = Arrays.asList(new BigDecimal(100), new BigDecimal(240));

        when(transferManager.getAllTransactions(ITSME, MSMITH)).thenReturn(list);

        strategy = new TotalStrategy("TOTAL-SENT-MSMITH", userManager, transferManager);

        String r = strategy.execute(A12);

        assertEquals(r, "340");

    }

    @Test
    public void happyWayMultipleUserTest() {

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.existsUser(MSMITH)).thenReturn(true);

        when(userManager.existsUser(FFRITZ)).thenReturn(true);

        List<BigDecimal> listMsmith = Arrays.asList(new BigDecimal(100), new BigDecimal(240));

        List<BigDecimal> listFfritz = Arrays.asList(new BigDecimal(1), new BigDecimal(937));

        when(transferManager.getAllTransactions(ITSME, MSMITH)).thenReturn(listMsmith);

        when(transferManager.getAllTransactions(ITSME, FFRITZ)).thenReturn(listFfritz);

        strategy = new TotalStrategy("TOTAL-SENT-MSMITH-FFRITZ", userManager, transferManager);

        String r = strategy.execute(A12);

        assertEquals(r, "340,938");

    }

    @Test
    public void noUserTest() {

        when(userManager.getUserNameForDeviceId(A12)).thenReturn(ITSME);

        when(userManager.existsUser(NOUSER)).thenReturn(false);

        List<BigDecimal> list = Arrays.asList(new BigDecimal(100), new BigDecimal(240));

        when(transferManager.getAllTransactions(ITSME, NOUSER)).thenReturn(list);

        strategy = new TotalStrategy("TOTAL-SENT-NOUSER", userManager, transferManager);

        String r = strategy.execute(A12);

        assertEquals(r, SMSStatus.NO_USER.getMessage());

    }

}