package com.wirecard.sms.stategy;

import com.wirecard.sms.strategy.GenericStrategy;
import com.wirecard.transactions.TransferManager;
import com.wirecard.transactions.TransferManagerImpl;
import com.wirecard.users.UserManager;
import com.wirecard.users.UserManagerImpl;
import org.testng.annotations.BeforeClass;

import static org.mockito.Mockito.mock;

public abstract class MockStrategyTest {

    protected static final String A12 = "A12";
    protected static final String ITSME = "ITSME";
    protected static final String MSMITH = "MSMITH";
    protected static final String NOUSER = "NOUSER";
    protected static final String FFRITZ = "FFRITZ";

    protected UserManager userManager;

    protected TransferManager transferManager;

    protected GenericStrategy strategy;

    @BeforeClass
    public void setUp() {
        userManager = mock(UserManagerImpl.class);
        transferManager = mock(TransferManagerImpl.class);
    }
}
