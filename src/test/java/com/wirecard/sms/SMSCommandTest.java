package com.wirecard.sms;

import com.wirecard.sms.exception.InvalidSMSCommandException;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


public class SMSCommandTest {

    @Test
    public void returnedByCompoundedAsExpected() throws InvalidSMSCommandException {
        String input = "SEND-100-FFRITZ";
        SMSCommand smsCommand = SMSCommand.of(input);
        assertEquals(smsCommand, SMSCommand.SEND);
    }

    @Test
    public void returnedBySingleAsExpected() throws InvalidSMSCommandException {
        String input = "BALANCE";
        SMSCommand smsCommand = SMSCommand.of(input);
        assertEquals(smsCommand, SMSCommand.BALANCE);
    }

    @Test(expectedExceptions = InvalidSMSCommandException.class)
    public void returnedExceptionByInexistentAsExpected() throws InvalidSMSCommandException {
        String input = "FREE";
        SMSCommand.of(input);
    }

}