package com.wirecard;

import com.wirecard.db.domain.*;
import com.wirecard.db.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;

import java.math.BigDecimal;

@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class TestScenario extends AbstractTestNGSpringContextTests {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected TransactionRepository transactionRepository;

    @Autowired
    protected SMSTransactionRepository smsTransactionRepository;

    @Autowired
    protected DeviceRepository deviceRepository;

    @Autowired
    protected DepositTransactionRepository depositTransactionRepository;

    @BeforeClass
    public void setUp() {

        User u1 = createUser("TEST1");
        User u2 = createUser("TEST2");
        User u3 = createUser("TEST3");

        createDevice("D1", u1);
        createDevice("D2", u2);
        createDevice("D3", u3);

        createDepositTransaction( 7000, u1);
        createDepositTransaction( 7000, u2);
        createDepositTransaction( 7000, u3);

        createSMSTransaction(3000, u1, u2);

        createSMSTransaction(82, u1, u3);

        createSMSTransaction(91, u1, u2);

        createSMSTransaction(789, u2, u3);
    }

    protected void createDepositTransaction(int amount, User u) {

        Transaction t = new Transaction();
        t.setUser(u);

        DepositTransaction dt = new DepositTransaction();
        dt.setAmount(new BigDecimal(amount));
        dt.setTransaction(t);

        depositTransactionRepository.save(dt);
    }

    protected void createSMSTransaction(int amount, User from, User to) {

        Transaction t = createTransaction(from);

        SMSTransaction st = new SMSTransaction();
        st.setAmount(new BigDecimal(amount));
        st.setTransaction(t);
        st.setUser(to);

        smsTransactionRepository.save(st);
    }

    protected Transaction createTransaction(User u) {
        Transaction t = new Transaction();
        t.setUser(u);

        return transactionRepository.save(t);
    }

    protected Device createDevice(String deviceId, User u) {
        Device d = new Device();
        d.setPublicId(deviceId);
        d.setUser(u);

        return deviceRepository.save(d);
    }

    protected User createUser(String username) {
        User u = new User();
        u.setUsername(username);

        return userRepository.save(u);
    }
}
