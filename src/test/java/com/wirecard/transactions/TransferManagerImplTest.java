package com.wirecard.transactions;

import com.wirecard.TestScenario;
import com.wirecard.db.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class TransferManagerImplTest extends TestScenario {

    @Autowired
    private TransferManager transferManager;

    @Test
    public void listTransactionTest() {
        List<BigDecimal> ts = transferManager.getAllTransactions("TEST1", "TEST2");

        assertEquals(2, ts.size());
    }

    @Test
    public void listTransactionZeroWhenInexistentSenderTest() {
        List<BigDecimal> ts = transferManager.getAllTransactions("TEST0", "TEST1");

        assertEquals(0, ts.size());
    }

    @Test
    public void listTransactionZeroWhenInexistentRecipientTest() {
        List<BigDecimal> ts = transferManager.getAllTransactions("TEST1", "TEST4");

        assertEquals(0, ts.size());
    }

    @Test
    public void listTransactionZeroWhenBothInexistentTest() {
        List<BigDecimal> ts = transferManager.getAllTransactions("TEST0", "TEST4");

        assertEquals(0, ts.size());
    }

    @Test
    public void listTransactionZeroWhenNeverTransactedTest() {
        List<BigDecimal> ts = transferManager.getAllTransactions("TEST3", "TEST1");

        assertEquals(0, ts.size());
    }

    @Test
    public void sendMoneyTest() {
        User u4 = createUser("TEST4");
        User u5 = createUser("TEST5");

        transferManager.sendMoney(u4.getUsername(), u5.getUsername(), new BigDecimal(900));

        List<BigDecimal> ts = transferManager.getAllTransactions(u4.getUsername(), u5.getUsername());

        assertEquals(1, ts.size());

        assertEquals(900, ts.get(0).intValue());
    }
}
