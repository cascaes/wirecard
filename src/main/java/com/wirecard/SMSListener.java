package com.wirecard;

import com.wirecard.rabbitmq.Receiver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@Profile("!test")
public class SMSListener implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    private final Receiver receiver;

    public SMSListener(Receiver receiver, RabbitTemplate rabbitTemplate) {
        this.receiver = receiver;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception{

        log.info("SMSListener started...");

        while (true) {
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            rabbitTemplate.convertAndSend(DefaultConfiguration.topicExchangeName, "wirecard.sms.command", input);
            receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
        }
    }
}
