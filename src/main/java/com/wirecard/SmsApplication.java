package com.wirecard;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SmsApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}
}
