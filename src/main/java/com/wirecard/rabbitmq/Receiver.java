package com.wirecard.rabbitmq;

import com.wirecard.sms.SMSHandler;
import com.wirecard.sms.strategy.SMSStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Slf4j
@Component
public class Receiver {

    @Autowired
    private SMSHandler smsHandler;

    private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message) {

        String[] data = message.split("\\s");
        log.info("RABBITMQ received - order: " + latch.getCount());
        try {
            System.out.println(smsHandler.handleSmsRequest(data[0], data[1]));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(SMSStatus.UNKNOWN.getMessage());
        }
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}