CREATE TABLE IF NOT EXISTS users (
    id          INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username    VARCHAR(1000) NOT NULL
);

CREATE TABLE IF NOT EXISTS devices (
    id          INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    public_id   VARCHAR(1000) NOT NULL,
    user_id     INTEGER NOT NULL,
    CONSTRAINT device_user_fk
    FOREIGN KEY(user_id)
    REFERENCES users(id),
    UNIQUE (public_id)
);

CREATE TABLE IF NOT EXISTS transactions (
	id          INTEGER      AUTO_INCREMENT  NOT NULL,
	user_id     INTEGER                      NOT NULL,
	created_at  DATETIME                    NOT NULL,
	CONSTRAINT transaction_user_fk
    FOREIGN KEY(user_id)
    REFERENCES users(id),
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS sms_transactions (
	id              INTEGER      AUTO_INCREMENT  NOT NULL,
	user_id         INTEGER                      NOT NULL,
    amount          DECIMAL(19, 2)               NOT NULL,
    transaction_id  INTEGER                      NOT NULL,
    CONSTRAINT sms_transaction_transaction_fk
    FOREIGN KEY(transaction_id)
    REFERENCES transactions(id),
    CONSTRAINT sms_transaction_user_fk
    FOREIGN KEY(user_id)
    REFERENCES users(id),
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS deposit_transactions (
	id              INTEGER      AUTO_INCREMENT  NOT NULL,
    amount          DECIMAL(19, 2)               NOT NULL,
    transaction_id  INTEGER                      NOT NULL,
    CONSTRAINT deposit_transaction_transaction_fk
    FOREIGN KEY(transaction_id)
    REFERENCES transactions(id),
	PRIMARY KEY(id)
);

INSERT INTO users (username) VALUES('ITSME');
INSERT INTO users (username) VALUES('FFRITZ');
INSERT INTO users (username) VALUES('MSMITH');

INSERT INTO devices (public_id, user_id) VALUES('A12', 1);
INSERT INTO devices (public_id, user_id) VALUES('G7', 2);
INSERT INTO devices (public_id, user_id) VALUES('MT105', 3);

INSERT INTO transactions (user_id, created_at) VALUES(1, NOW());
INSERT INTO transactions (user_id, created_at) VALUES(2, NOW());
INSERT INTO transactions (user_id, created_at) VALUES(3, NOW());

INSERT INTO transactions (user_id, created_at) VALUES(1, NOW());
INSERT INTO transactions (user_id, created_at) VALUES(1, NOW());
INSERT INTO transactions (user_id, created_at) VALUES(2, NOW());
INSERT INTO transactions (user_id, created_at) VALUES(3, NOW());

INSERT INTO deposit_transactions (amount, transaction_id) VALUES('5000.00', 1);
INSERT INTO deposit_transactions (amount, transaction_id) VALUES('5000.00', 2);
INSERT INTO deposit_transactions (amount, transaction_id) VALUES('5000.00', 3);

INSERT INTO sms_transactions (user_id, amount, transaction_id) VALUES(2, '500.00', 4);
INSERT INTO sms_transactions (user_id, amount, transaction_id) VALUES(3, '50.00', 5);
INSERT INTO sms_transactions (user_id, amount, transaction_id) VALUES(1, '50.00', 6);
INSERT INTO sms_transactions (user_id, amount, transaction_id) VALUES(1, '50.00', 7);

