package com.wirecard.users;

import com.wirecard.db.domain.*;
import com.wirecard.db.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private SMSTransactionRepository smsTransactionRepository;

    @Autowired
    private DepositTransactionRepository depositTransactionRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    public boolean existsUser(String username) {
        Optional<User> userOpt = userRepository.findByUsername(username);
        return userOpt.isPresent();
    }

    @Override
    public BigDecimal getBalance(String username) {
        Optional<User> userOpt = userRepository.findByUsername(username);

        List<Transaction> listTransactions;
        List<SMSTransaction> listSentSMSTransactions = new ArrayList<>();
        List<SMSTransaction> listReceivedSMSTransactions = new ArrayList<>();
        List<DepositTransaction> listDepositTransactions = new ArrayList<>();

        if (userOpt.isPresent()) {
            listTransactions = transactionRepository.findAllByUser(userOpt.get());
            listSentSMSTransactions = smsTransactionRepository.findAllByTransactionIn(listTransactions);
            listReceivedSMSTransactions = smsTransactionRepository.findAllByUser(userOpt.get());
            listDepositTransactions = depositTransactionRepository.findAllByTransactionIn(listTransactions);
        }

        BigDecimal spents = BigDecimal.ZERO;
        for (SMSTransaction spent : listSentSMSTransactions) {
            spents = spents.add(spent.getAmount());
        }

        BigDecimal gains = BigDecimal.ZERO;
        for (DepositTransaction gain : listDepositTransactions) {
            gains = gains.add(gain.getAmount());
        }
        for (SMSTransaction gain : listReceivedSMSTransactions) {
            gains = gains.add(gain.getAmount());
        }

        return gains.subtract(spents);
    }

    @Override
    public String getUserNameForDeviceId(String deviceId) {
        Optional<Device> deviceOpt = deviceRepository.findByPublicId(deviceId);
        return deviceOpt.map(d -> d.getUser().getUsername()).orElse(null);

    }
}
