package com.wirecard.transactions;

import com.wirecard.db.domain.SMSTransaction;
import com.wirecard.db.domain.Transaction;
import com.wirecard.db.domain.User;
import com.wirecard.db.repository.SMSTransactionRepository;
import com.wirecard.db.repository.TransactionRepository;
import com.wirecard.db.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TransferManagerImpl implements TransferManager {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private SMSTransactionRepository smsTransactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void sendMoney(String senderUsername, String recipientUsername, BigDecimal amount) {
        Optional<User> userRecipientOpt = userRepository.findByUsername(recipientUsername);
        Optional<User> userSenderOpt = userRepository.findByUsername(senderUsername);

        if (!userRecipientOpt.isPresent() || !userSenderOpt.isPresent()) {
            return;
        }

        Transaction transaction = new Transaction();
        transaction.setUser(userSenderOpt.get());
        transactionRepository.save(transaction);

        SMSTransaction smsTransaction = new SMSTransaction();
        smsTransaction.setAmount(amount);
        smsTransaction.setUser(userRecipientOpt.get());
        smsTransaction.setTransaction(transaction);

        smsTransactionRepository.save(smsTransaction);

    }

    @Override
    public List<BigDecimal> getAllTransactions(String senderUsername, String recipientUsername) {
        Optional<User> userRecipientOpt = userRepository.findByUsername(recipientUsername);
        Optional<User> userSenderOpt = userRepository.findByUsername(senderUsername);

        if (!userRecipientOpt.isPresent() || !userSenderOpt.isPresent()) {
            return new ArrayList<>();
        }

        List<SMSTransaction> list = smsTransactionRepository.findAllByUser(userRecipientOpt.get());

        return list.stream()
                .filter(t -> t.getTransaction().getUser().equals(userSenderOpt.get()))
                .map(SMSTransaction::getAmount)
                .collect(Collectors.toList());
    }
}
