package com.wirecard.db.repository;

import com.wirecard.db.domain.Transaction;
import com.wirecard.db.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByUser(User user);
}
