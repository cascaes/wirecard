package com.wirecard.db.repository;

import com.wirecard.db.domain.SMSTransaction;
import com.wirecard.db.domain.Transaction;
import com.wirecard.db.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SMSTransactionRepository extends JpaRepository<SMSTransaction, Long> {
    List<SMSTransaction> findAllByTransactionIn(List<Transaction> listTransactions);

    List<SMSTransaction> findAllByUser(User user);
}
