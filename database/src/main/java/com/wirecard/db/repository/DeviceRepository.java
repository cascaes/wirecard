package com.wirecard.db.repository;

import com.wirecard.db.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeviceRepository extends JpaRepository<Device, Long> {
    Optional<Device> findByPublicId(String deviceId);

}
