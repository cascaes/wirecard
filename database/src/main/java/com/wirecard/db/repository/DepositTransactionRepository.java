package com.wirecard.db.repository;

import com.wirecard.db.domain.DepositTransaction;
import com.wirecard.db.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepositTransactionRepository extends JpaRepository<DepositTransaction, Long> {
    List<DepositTransaction> findAllByTransactionIn(List<Transaction> listTransactions);

}
