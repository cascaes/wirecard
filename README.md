# 1. Instruction to run
- `git clone git@bitbucket.org:cascaes/wirecard.git`
- `cd wirecard`
- `./gradlew build`
- Turn on the RabbitMQ and Mariadb with `docker-compose up -d` (install the Docker before [here](https://docs.docker.com/install/))
- `java -jar build/libs/wirecard-1.0.0.jar` - Minimum Java: 8^

P.S.: Application will start with predefined SQL data. Try with `BALANCE A12`.

Then you may play with it using the **SMS Transactions Commands**

# 2. Run the tests
- `./gradlew clean`
- `./gradlew test`

# 3. Assignment Requirements
#### 1. The command codes will behave as described in the above table.

   **DONE**

#### 2. The module will be extended in the future with new command types. Ensure that the solution is easily extended.

   To extend, only three actions will be required:

   2.1.  Add a new ENUM in `SMSCommand.java` that represents the new command, with same name itself

   2.2. Extends the `GenericStrategy.java` class and implement the new class - an application of *Strategy Pattern*

   2.3. Finally, just add the new `case` option in the `switch` flow control inside the `SMSHandlerImpl.java`

#### 3. Adequate logging should be performed to allow operations staff to monitor the system once it goes into production.

   **DONE**

#### 4. The implementation should be well tested.

   **DONE** - focusing in the business

#### 5. The implementation should scale to a large number of users.

   **DONE** - A JMS was added - using the RabbitMQ - to avoid concurrency problem when many transactions is occurring in same time, then all
    transactions are queued

# 4. Development

- 4 modules were created - one additional is the _database_ - to give enough data structure to the implementation, others are
_users_, _transactions_ and _SMS_
- **Strategy Pattern** is used to manage incoming SMS Command, as almost each command commonly uses the
`UserManager` and `TransferManager` interfaces but differs greatly the action meaning
- **Gradle** is used to package management as this has better empowerment and is cleaner
- **TestNG** was used instead of **JUnit** because it's better handled about reusage and scope of testing
- **Spring Data** is used to allow less harm possible when changing a DBMS would be needed